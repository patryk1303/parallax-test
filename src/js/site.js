import ParallaxSite from './parallax'

document.addEventListener('DOMContentLoaded', function() {
  const parallaxEl = document.querySelector('.parallax-site')
  const scrollPage = new ParallaxSite(parallaxEl)
})