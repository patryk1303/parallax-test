import {template} from 'lodash'
import $ from 'jquery'

class ParallaxSite {
  /**
   * Creates parallax site
   * @param {HTMLElement} el Parallax site
   */
  constructor(el) {
    this.el = el
    this.data = el.dataset

    this.bindDom()
    this.bindData()
    this.bindEvents()

    this.buildMenu()
    this.positionMenu()
  }

  bindDom() {
    this.parallaxSections = this.el.querySelectorAll('.parallax-site__section')
    this.floatNav = this.el.querySelector('.float-nav')
    this.floatNavToggle = this.floatNav.querySelector('.float-nav__toggle')
    this.floatNavList = this.floatNav.querySelector('.float-nav__list')
  }

  bindData() {
    this.menu = []
    this.currentSection = null

    this.isMobile = false

    for(let el of this.parallaxSections) {
      let title = el.dataset.title
      let id = title.toLowerCase().replace(/[#() -]/gi, '_')

      el.setAttribute('id', id)

      let menuItem = {
        title,
        id
      }

      this.menu.push(menuItem)
    }
  }

  bindEvents() {
    window.addEventListener('scroll', this.handleWindowScroll.bind(this))
    
    window.addEventListener('resize', () => {
      this.positionMenu()
      this.handleWindowScroll() 
    })

    this.floatNavList.addEventListener('click', this.handleFloatNavListClick.bind(this))
    this.floatNavToggle.addEventListener('click', this.handleFloatNavToggleClick.bind(this))

    this.isMobileMatch = window.matchMedia('screen and (max-width: 768px)')

    if(this.isMobileMatch.matches) {
      this.isMobile = true
    } else {
      this.isMobile = false
    }

    this.isMobileMatch.addEventListener('change', (ev) => {
      if(ev.matches) {
        this.isMobile = true
      } else {
        this.isMobile = false
      }
    })
  }

  buildMenu() {
    let menuHtml = ''
    let menuItemTemplate = `<li class="float-nav__element">
      <a href="#<%= id %>" class="float-nav__link"><%= title %></a>
    </li>`

    let compiledTemplate = template(menuItemTemplate)

    for(let menuItem of this.menu) {
      let itemHtml = compiledTemplate(menuItem)
      
      menuHtml += itemHtml
    }

    this.floatNavList.innerHTML = menuHtml
  }

  positionMenu() {
    let elOffset = this.el.offsetLeft

    this.floatNav.style.right = `${elOffset}px`
  }

  trackCurrentSectionInMenu() {
    const floatNavListLink = this.floatNavList.querySelectorAll('.float-nav__link')
    const sectionId = this.currentSection.getAttribute('id')
    
    for(let el of floatNavListLink) {
      el.classList.remove('float-nav__link--current')
      if(el.getAttribute('href') === `#${sectionId}`) {
        el.classList.add('float-nav__link--current')
      }
    }
  }

  getCurrentSection() {
    let scrolledHeight = window.pageYOffset
  
    for(let el of this.parallaxSections) {
      let limit = el.offsetTop + el.offsetHeight
  
      if(scrolledHeight >= el.offsetTop && scrolledHeight < limit) {
        if(this.currentSection !== el) {
          this.currentSection = el
          this.trackCurrentSectionInMenu()
        }
      }
    }
  }

  handleFloatNavListClick(e) {
    let target = e.target

    if (!target instanceof HTMLElement) {
      return
    }

    let isElement = target.classList.contains('float-nav__element')
    let isLink = target.classList.contains('float-nav__link')
    let href
    
    if (isElement) {
      let link = target.querySelector('.float-nav__link')
      href = link.getAttribute('href')
    } else if(isLink) {
      href = target.getAttribute('href')
    } else {
      return
    }
    
    let hrefEl = document.querySelector(href)
    
    if(!hrefEl) {
      return
    }

    e.preventDefault()

    let usedTop = hrefEl.offsetTop

    $('html, body').animate({
      scrollTop: usedTop
    }, 800, 'swing')
  }

  handleFloatNavToggleClick() {
    const OPENED_CLASS = 'float-nav--opened'

    if(this.floatNav.classList.contains(OPENED_CLASS)) {
      this.floatNav.style.width = ''
      this.floatNav.classList.remove(OPENED_CLASS)
    } else {
      let navElementWidths = []
      let floatNavLinks = this.floatNavList.querySelectorAll('.float-nav__link')
      
      for(let linkEl of floatNavLinks) {
        navElementWidths.push(linkEl.scrollWidth + this.floatNavList.scrollWidth)
      }

      let floatNavWidth = Math.max(...navElementWidths)
      this.floatNav.style.width = `${floatNavWidth}px`

      this.floatNav.classList.add(OPENED_CLASS)
    }
  }

  handleWindowScroll() {
    let scrolledHeight = window.pageYOffset

    if(this.isMobile) {
      for(let el of this.parallaxSections) {
        el.style.transform = 'translateY(0)'
      }
    } else {
      for(let el of this.parallaxSections) {
        let limit = el.offsetTop + el.offsetHeight
        let fig = el.querySelector('.parallax__figure')
  
        if(scrolledHeight > el.offsetTop && scrolledHeight <= limit) {
          let posY = `${(scrolledHeight - el.offsetTop) / 1.5}px`
          
          el.style.transform = `translateY(${posY})`
        } else {
          el.style.transform = `translateY(0)`
        }
      }
    }

    this.getCurrentSection()
  }
}

export default ParallaxSite